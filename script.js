console.log("New day world!");

// [SECTION] Objects

// An object is a data type that is  used to represent real world objects
// Is a collection of related data and or functionalities.

/*

	- Syntax
		let objectName = {
			keyA: valueA
			keyB: valueB
		}
// use {} for objects.
*/

let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}

console.log("Result from creating objects using initializers/literal notations"); // initializers and literal notation = {}
console.log(cellphone);

// "this" inside an object
function Laptop(name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constructors.")
console.log(laptop);
// object constructors --> "this" and "new."

// Re-using laptop function
let myLaptop = new Laptop ("MacBook Air", 2020)

console.log("Result from creating objects using object constructors.")
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log(oldLaptop); // will result to undefined because "new" was not used.

// Creating empty object
let computer = {}
let myComputer = new Object();

// [SECTION] Accessing Object Properties.
// DOT NOTATION --> to access particular property in an object.

console.log("Result from dot notation: " + myLaptop.name);

// Square bracket notation
console.log("Result form square bracket notation: " + myLaptop['name']);

// Accessing array objects

let array = [laptop, myLaptop];

// accessing array using indexes.
console.log(array[0]['name']);
console.log(array[0].name);

// [SECTION] Initializing, Adding, Deleting, Re-assigning Object Properties

let car = {};

// Hoisting -->
console.log(car);

//Initializing or Adding property --> using dot notation

car.name = "Honda Civic";
console.log("Result of adding a property using dot notation:");
console.log(car);

// car.manufacturedDate = 2019;
// console.log("Result from adding a propert using dot notation:");
// console.log(car);

// Initializing or Adding property using bracket notation

car['manufacturedDate'] = 2019;
console.log(car);
console.log("Result from adding a propert using bracket notation:");
console.log(car.manufacturedDate);

// deleTe object property
// delete car ['manufacturedDate'];
delete car.manufacturedDate;  //['manufacturedDate'];
console.log("Result from deleting properties: ")
console.log(car);

// Re-assigning object properties --> modifying/updating
car.name  = "Dodge Charger R/T";
console.log("Result from re-assigning properties");
console.log(car);

// [SECTION] Object Methods
// is a function which is a property of an object

let person = {
	name: "Al",
	talk: function(){
		console.log("Hello my name is " + this.name + ".");
	}
}

console.log(person);
console.log("Result of object method");
person.talk(); // invocation/calling a function
// to invoke - call the object.property with a function();

// Add a property with an object method function
person.walk = function () {
	console.log(this.name + " walked 25 steps forward.");
}
person.walk();

// another Example.

let friend = {
	firstName: "Joe",
	lastName: "Momma",
	// NESTED OBJECTS
	address: {
		houseID: 42,
		street: "3rd Street",
		block: "2nd",
		city: "Austin",
		state: "Texas",
		country: "United States"
	},
	// Nested array within an object property
	emails: ["joewho@mail.com","joemomma@mail.com"],
	introduce: function () {
		console.log("Hello my name is " + this.firstName + " " + this.lastName);
	}
}
friend.introduce();
 
// [SECTION] Real world application of objects
/*
	- Scenario
		1. We would like to create a game that would have several pokemopn interact with each other.
		2. Every pokemon would have the same set of stats, properties and functions
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function () {
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	},
	faint: function () {
		console.log("Pokemon fainted!");
	}
}

console.log(myPokemon);

// Creating an object constructor to use on the process "this" and "new".

function Pokemon(name, level, health, attack) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level; // 32
	this.attack = level; // 3

	//Methods/Function
	this.tackle = function (target) {
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now _targetPokemonHealth_");
	}
	this.faint = function () {
		console.log(this.name + " fainted!");
	}
	
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);